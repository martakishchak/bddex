package steps;

import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.AfterStep;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import manager.WebDriverManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.CartPage;
import pages.HomePage;
import pages.ProductPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class MyStepdefs {

    PageFactoryManager pageFactoryManager = new PageFactoryManager();

    HomePage homePage;
    ProductPage productPage;
    CartPage cartPage;

    static Logger log = Logger.getLogger(MyStepdefs.class);

    @BeforeAll
    public static void beforeSuite(){
        PropertyConfigurator.configure("src/main/resources/log4j.properties");

    }

    /*@BeforeTest
    public void beforeTests() {
        //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeMethod
    public void beforeMethod (){

    }

     */

    @After
    public static void afterMethod(){
        WebDriverManager.getDriver().manage().deleteAllCookies();
    }

    @AfterAll
    public static void afterTests() {
        // webDriver.quit();
        WebDriverManager.quitAll();
    }

    @io.cucumber.java.en.Given("^User opens 'https://rozetka\\.com\\.ua/ua/' page$")
    public void userOpensHttpsRozetkaComUaUaPage() {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage_BO();
    }

    @And("User input {string} in product input box")
    public void userInputProductInProductInputBox(String productName) {
        homePage.inputProductName_BO(productName);
    }

    @And("User input {string} in brand input box")
    public void userInputBrandInBrandInputBox(String brandName) {
        productPage = pageFactoryManager.getProductPage();
        productPage.inputBrandName_BO(brandName);
    }

    @And("User sorts products from expensive to cheap")
    public void userSortsProductsFromExpensiveToCheap() throws InterruptedException {
       // Thread.sleep(5000);
        productPage.sortFromExpensiveToCheap_BO();
        Thread.sleep(5000);
    }

    @And("User adds first product from products list to cart")
    public void userAddsFirstProductFromProductsListToCart() {
        productPage.addToCartFirstProduct_BO();
    }

    @And("User opens cart page")
    public void userOpensCartPage() {
        productPage.clickOnCartPage_BO();
        cartPage = pageFactoryManager.getCartPage();
    }

    @Then("User checks if price in cart page is higher than {string}")
    public void userChecksIfPriceInCartPageIsHigherThanPrice(String price) {
        int priceTmp = cartPage.getCartSumm_BO();
        Assert.assertTrue(priceTmp > Integer.parseInt(price));
    }
}
