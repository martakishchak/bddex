Feature: simple
As a User
I want to test some simple functionality
So that I can be sure everything works correctly

Scenario Outline: check if a price of first product is appropriate
  Given User opens 'https://rozetka.com.ua/ua/' page
  And User input '<product>' in product input box
  And User input '<brand>' in brand input box
  And User sorts products from expensive to cheap
  And User adds first product from products list to cart
  And User opens cart page
  Then User checks if price in cart page is higher than '<price>'

  Examples:
  | product     | brand     | price |
  | Laptop      | HP        | 50000 |
  | Bike        | Make Bike | 15000 |
  |  Book       | Vivat     | 50    |
  |  Shampoo    | Hillary   | 100   |
  |  Shoes      | FX shoes  | 1000  |