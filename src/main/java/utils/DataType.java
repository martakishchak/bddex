package utils;

public class DataType {
    private String product;
    private String brand;
    private int price;

    public String getProduct(){return this.product;}
    public String getBrand(){return this.brand;}
    public int getPrice(){return  this.price;}

    @Override
    public String toString() {
        return "DataType{" +
                "product='" + product + '\'' +
                ", brand='" + brand + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
