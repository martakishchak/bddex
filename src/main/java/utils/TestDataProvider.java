package utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
//import jdk.internal.org.objectweb.asm.TypeReference;
import org.bouncycastle.util.test.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class  TestDataProvider {
    public static List<DataType> getDataFromJson(String fileName) throws IOException {

        final ObjectMapper objectMapper = new ObjectMapper();
        List<DataType> langList = objectMapper.readValue(
                new File(fileName),
                new TypeReference<List<DataType>>(){});

    //    langList.forEach(x -> System.out.println(x.toString()));
        return langList;
    }


    public static void main(String[] args) throws IOException {
        System.out.println(TestDataProvider.getDataFromJson("src\\main\\resources\\TestData.json"));
    }


}
