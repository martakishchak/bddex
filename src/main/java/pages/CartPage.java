package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage{
    public CartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='cart-receipt__sum-price']/span")
    private WebElement priceElement;

    public WebElement getPrice(){
        waitVisibilityOfElement(priceElement, 5);
        return priceElement;
    }

    @FindBy(xpath = "//button[@id='cartProductActions0']/../div/ul/li/rz-trash-icon/button[@class='button button--medium button--with-icon button--link context-menu-actions__button']")
    private  WebElement removeFromCartButton;
    public WebElement getRemoveFromCartButton(){return removeFromCartButton;}

    public int getCartSumm_BO(){
        return Integer.parseInt(getPrice().getText());
    }

}
