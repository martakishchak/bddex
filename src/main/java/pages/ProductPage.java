package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ProductPage extends BasePage {

    public ProductPage(WebDriver driver){super(driver);}

    @FindBy(xpath = "//div[@data-filter-name='producer']//input[@type='search' and @class='sidebar-search__input ng-untouched ng-pristine ng-valid']")
    private WebElement brandInput;
    public WebElement getBrandInput(){
        waitVisibilityOfElement(brandInput, 5);
        return brandInput;
    }

    @FindBy(xpath = "//div[@class='sidebar-block ng-star-inserted' and @data-filter-name='producer']/div[@class='sidebar-block__inner ng-star-inserted']/rz-scrollbar/div/div/div/div/rz-filter-checkbox/ul/li/a")
    private List<WebElement> brandCheckBoxList;
    public List<WebElement> getBrandCheckBox(){return brandCheckBoxList;}
    public WebElement getBrandCheckBox(int index){return brandCheckBoxList.get(index);}
    public WebElement getBrandCheckBox(String brandName){
        //By brandSelector = By.xpath("//div[@class='sidebar-block ng-star-inserted' and @data-filter-name='producer']/div[@class='sidebar-block__inner ng-star-inserted']/rz-scrollbar/div/div/div/div/rz-filter-checkbox/ul/li/a[text()='" + brandName + "']");
        By brandSelector = By.xpath("//div[@data-filter-name='producer']//a[@class='checkbox-filter__link' and @data-id='" + brandName + "']");
        waitVisibilityOfElements(brandSelector, 5);
        return driver.findElement(brandSelector);
        /*
        for (WebElement brandItem: brandCheckBoxList
             ) {
            if(brandItem.getText().contains(brandName))
                return brandItem;
        }
        return null;
        */
    }



    //@FindBy(xpath = "//select[@class='select-css ng-pristine ng-valid ng-star-inserted ng-touched']/option[@value='2: expensive']")
    @FindBy(xpath = "//select[@class='select-css ng-untouched ng-pristine ng-valid ng-star-inserted']")
    private  WebElement selectExpencive;
    public  WebElement getSelectExpensive(){
        waitVisibilityOfElement(selectExpencive, 5);
        return  selectExpencive;
    }

    @FindBy(xpath = "//div[@class='goods-tile__inner']")
    List<WebElement> resultItems;
    public List<WebElement> getResultItems(){return resultItems;}

    By addToCartButtonsLocator = By.xpath("//button[@class='buy-button goods-tile__buy-button ng-star-inserted']");

    public List<WebElement> getAddToCartButtons(){
        waitVisibilityOfElements(addToCartButtonsLocator, 5);
        return driver.findElements(addToCartButtonsLocator);
    }

    @FindBy(xpath = "//button[@class='header__button ng-star-inserted header__button--active']")
    private WebElement cartButton;
    public WebElement getCartButton(){
        waitVisibilityOfElement(cartButton, 5);
        return cartButton;
    }

    public void inputBrandName_BO(String brandName){
        waitForPageLoadComplete(5);
        logPages.info("Get productPage");
        waitVisibilityOfElement(getBrandCheckBox(brandName), 3);
        getBrandCheckBox(brandName).click();
        logPages.info("Search brandName in the list of brands and select correct one and click");
    }

    public void sortFromExpensiveToCheap_BO(){
        waitVisibilityOfElement(getSelectExpensive(), 3);
        Select dropdown = new Select(getSelectExpensive());
        logPages.info("Click on selector dropDown");
        dropdown.selectByIndex(2);
        logPages.info("Select sorting from expensive to cheep");
    }

    public void addToCartFirstProduct_BO(){
        waitVisibilityOfElement(getResultItems().get(0), 3);
        getAddToCartButtons().get(0).click();
        logPages.info("Select the most expensive product and add it to the cart");
    }

    public void clickOnCartPage_BO(){
        waitVisibilityOfElement(getCartButton(), 3);
        getCartButton().click();
        logPages.info("Click on the cart Button");
    }

}
