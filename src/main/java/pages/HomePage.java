package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.ConfigFileReader;

public class HomePage extends BasePage{

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@class='search-form__input ng-untouched ng-pristine ng-valid']")
    private WebElement searchBox;

    public  WebElement getSearchBox(){return searchBox;}

    public void openHomePage() {
        driver.get(ConfigFileReader.getInstance().getHomePageUrl());
    }

    public void openHomePage_BO(){
        openHomePage();
        logPages.info("Open HomePage");
        logPages.info(Thread.currentThread().getId());
        waitForPageLoadComplete(3);
    }

    public void inputProductName_BO(String productName){
        getSearchBox().sendKeys(productName + Keys.ENTER);
        logPages.info("Get Product SearchBox and Enter productName + \"ENTER\"");
    }
}
