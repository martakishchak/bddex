package manager;

import pages.CartPage;
import pages.HomePage;
import pages.ProductPage;
import pages.ResultPage;

public class PageFactoryManager {
    public PageFactoryManager() {
    }

    public HomePage getHomePage(){return new HomePage(WebDriverManager.getDriver());}
    public CartPage getCartPage(){return new CartPage(WebDriverManager.getDriver());}
    public ProductPage getProductPage(){return new ProductPage(WebDriverManager.getDriver());}
    public ResultPage getResultPage(){return new ResultPage(WebDriverManager.getDriver());}

}
