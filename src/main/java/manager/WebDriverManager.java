package manager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.ConfigFileReader;

import java.util.HashMap;
import java.util.Map;

public class WebDriverManager {
    private static final Map<Long, WebDriver> webDrivers = new HashMap<>();

    public static WebDriver getDriver() {
        Long threadId = Thread.currentThread().getId();
        if(webDrivers.get(threadId) == null) {
            webDrivers.put(threadId, createNewDriver());
        }

        return webDrivers.get(threadId);
    }

    private static WebDriver createNewDriver(){
        System.setProperty("webdriver.chrome.driver", ConfigFileReader.getInstance().getDriverPath() );
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        return webDriver;
    }

    public static void quitAll(){
        for (WebDriver w:webDrivers.values()) {
            w.quit();

        }
    }
}
